<?php

namespace App\Http\Controllers;

use App\TagNews;
use Illuminate\Http\Request;

class TagNewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TagNews  $tagNews
     * @return \Illuminate\Http\Response
     */
    public function show(TagNews $tagNews)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TagNews  $tagNews
     * @return \Illuminate\Http\Response
     */
    public function edit(TagNews $tagNews)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TagNews  $tagNews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TagNews $tagNews)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TagNews  $tagNews
     * @return \Illuminate\Http\Response
     */
    public function destroy(TagNews $tagNews)
    {
        //
    }
}
