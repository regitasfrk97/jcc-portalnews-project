@extends('layout.main')

@section('container')
<div class="container">
	<ul class="list-group">
		<li class="list-group-item d-flex justify-content-between align-items-center">
		    <form method="POST" action="/tags" class="form-inline">
	            @csrf
	            <div class="form-group">
	                <label for="name">Tag Title</label>
	            </div>
	            <div class="form-group">
	            	<input type="text" name="name" class="form-control">
	            </div>
	            <input type="submit" name="submit" class="btn btn-success pull-right">            
	        </form>
		</li>
		@foreach($tags as $t)
			<li class="list-group-item d-flex justify-content-between align-items-center">
			    <?= $t->name ?>
			    <form method="POST" action="/tags/{{$t->id}}" class="pull-right">
		            @method('DELETE')
		            @csrf
		            <button type="submit" class="btn btn-sm" onclick="return confirm('Are you sure to delete this item?')"><i class="icon-trash"></i></button>
		        </form>
		        <a href="/tags/edit/{{$t->id}}" class="btn btn-sm pull-right"><i class="icon-pencil"></i></a>
			</li>
		@endforeach
	</ul>
</div>
@endsection