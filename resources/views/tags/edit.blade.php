@extends('layout.third')

@section('container')
<div class="container">
    <div class="col-md-8">
        <form method="POST" action="/tags/{{$tags->id}}">
            @method('PUT')
            @csrf
            <input type="hidden" name="id" value="{{$tags->id}}">
            <div class="form-group">
                <label for="name">Tag Title</label>
                <input type="text" name="name" class="form-control" value="{{$tags->name}}">
            </div>
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-success" >
            </div>            
        </form>
    </div>
</div>
@endsection