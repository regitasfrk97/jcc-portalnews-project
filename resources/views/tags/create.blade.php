@extends('layout.second')

@section('container')
<div class="container">
    <div class="col-md-8">
        <form method="POST" action="/tags">
            @csrf
            <div class="form-group">
                <label for="name">Tag Title</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-success">
            </div>            
        </form>
    </div>
</div>
@endsection