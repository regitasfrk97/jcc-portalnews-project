@extends('layout.main')

@section('container')
<div class="container">
	<div class="list-group">
		@foreach($news as $n)
			<a href="/news/{{ $n->id }}" class="list-group-item list-group-item-action"><?= $n->title ?></a>
		@endforeach
	</div>
</div>
@endsection