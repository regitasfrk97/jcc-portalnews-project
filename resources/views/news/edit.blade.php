@extends('layout.third')

@section('container')
<div class="container">
    <div class="col-md-8">
        <form method="POST" action="/news/{{$news->id}}">
            @method('PUT')
            @csrf
            <input type="hidden" name="id" value="{{$news->id}}">
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" name="title" class="form-control" value="{{$news->title}}">
            </div>
            <div class="form-group">
                <label for="content">Isi</label>
                <br>
                <textarea name="content"  rows="10" cols="50">{{$news->content}}</textarea>
            </div>
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-success" >
            </div>            
        </form>
    </div>
</div>
@endsection