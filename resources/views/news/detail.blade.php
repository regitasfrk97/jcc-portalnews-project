@extends('layout.second')

@section('container')
<div class="container">
    <div class="col-md-8">
        <h2>{{ $news->title }}</h2>
        <p>{{ $news->content }}</p>
        <a href="/news/edit/{{$news->id}}" class="btn btn-sm btn-warning">Edit</a>
        <form method="POST" action="/news/{{$news->id}}">
            @method('DELETE')
            @csrf
            <input type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete this item?')" value="Delete">
        </form>
    </div>
</div>

@endsection