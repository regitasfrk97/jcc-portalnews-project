@extends('layout.second')

@section('container')
<div class="container">
    <div class="col-md-8">
        <form method="POST" action="/news">
            @csrf
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" name="title" class="form-control">
            </div>
            <div class="form-group">
                <label for="content">Isi</label>
                <br>
                <textarea name="content" rows="10" cols="50"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-success">
            </div>            
        </form>
    </div>
</div>
@endsection