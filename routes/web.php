<?php
use App\Http\Controllers\NewsController;
use App\Http\Controllers\TagsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/', function () {
//     return view('news_list');
// });
// Route::get('/news', function () {
//     return view('news_list');
// });
// Route::get('/news/1', function () {
//     return view('news');
// });

Route::get('/', 'NewsController@index');
Route::get('/add_news', 'NewsController@create');
Route::get('/news/{news_id}', 'NewsController@show');
Route::post('/news', 'NewsController@store');
Route::get('/news/edit/{news_id}', 'NewsController@edit');
Route::put('/news/{news_id}', 'NewsController@update');
Route::delete('/news/{news_id}', 'NewsController@destroy');

Route::get('/tag_list', 'TagsController@index');
Route::get('/add_tag', 'TagsController@create');
Route::post('/tags', 'TagsController@store');
Route::get('/tags/edit/{tags_id}', 'TagsController@edit');
Route::put('/tags/{tags_id}', 'TagsController@update');
Route::delete('/tags/{tags_id}', 'TagsController@destroy');
